using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// This script automatically connects to Photon (using the settings file), 
/// tries to join a random room and creates one if none was found (which is ok).
/// </summary>
public class Network : Photon.MonoBehaviour
{
    private GamePlay iGamePlay;
    private UI iUI;
    public string strVersion = "0.5";

    private void Init() {
        iGamePlay = GamePlay.GetInstance();
        iUI = UI.GetInstance();
    }

    public virtual void Start()
    {
        Init();

        PhotonNetwork.autoJoinLobby = false;    // we join randomly. always. no need to join a lobby to get the list of rooms.
    }

    public virtual void Update()
    {
    }

    public void Connect() {
        if (!PhotonNetwork.connected) {
            iUI.ShowNetworkMessage("Connecting Server...");
            PhotonNetwork.ConnectUsingSettings(strVersion);
        }
    }

    // to react to events "connected" and (expected) error "failed to join random room", we implement some methods. PhotonNetworkingMessage lists all available methods!

    public virtual void OnConnectedToMaster()
    {
/*
        if (PhotonNetwork.networkingPeer.AvailableRegions != null) {
            Debug.LogWarning("List of available regions counts " + PhotonNetwork.networkingPeer.AvailableRegions.Count + ". First: " + PhotonNetwork.networkingPeer.AvailableRegions[0] + " \t Current Region: " + PhotonNetwork.networkingPeer.CloudRegion);
        }
*/
        iUI.ShowNetworkMessage("Connected Server. Finding a Room...");
        PhotonNetwork.JoinRandomRoom();
    }

    public virtual void OnPhotonRandomJoinFailed()
    {
        iUI.ShowNetworkMessage("No Available Room. Creating a Room..."); 
        PhotonNetwork.CreateRoom(null, new RoomOptions() { maxPlayers = 2 }, null);
    }

    // the following methods are implemented to give you some context. re-implement them as needed.

    public virtual void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        iUI.ShowNetworkMessage("Connect Faild ("+ cause + ")"); 
    }

    public void OnJoinedRoom()
    {
        if (PhotonNetwork.room.playerCount == 1)
        {
            iUI.ShowNetworkMessage("Waiting for Another Player...");
            PhotonNetwork.SetMasterClient(PhotonNetwork.player);
            iGamePlay.SetHostPlayer(PhotonNetwork.isMasterClient);

            iUI.ShowNetworkMessageForDebug("Created a Room. Host : " + PhotonNetwork.isMasterClient);
        }
        else
        {
            iGamePlay.SetHostPlayer(PhotonNetwork.isMasterClient);
            iUI.ShowNetworkMessage("Joined a Room. Now Start Playing...");

            if (PhotonNetwork.isMasterClient == false)
            {
                photonView.RPC("Remote_StartPlay", PhotonTargets.Others, "");
            }

            iUI.ShowNetworkMessageForDebug("Joined a Room. Host : " + PhotonNetwork.isMasterClient);
        }
    }

    public void OnPhotonPlayerDisconnected()
    {
        if (GameManager.GetInstance().GetGameState() == GameManager.GAME_STATE.PLAY)
        {
            iUI.ShowNetworkMessage("Opponent was disconnected...");
        }
        iGamePlay.MultiPlay_Disconnected();
    }

    public void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }

    public void OnJoinedLobby()
    {
//        Debug.Log("OnJoinedLobby(). Use a GUI to show existing rooms available in PhotonNetwork.GetRoomList().");
    }

    public void SendCardDeck(CardDeck iCardDeck)
    {
        iUI.ShowNetworkMessageForDebug("call SendCardDeck");
        string strCardSequence = iCardDeck.GetShuffleCards();
        photonView.RPC("Remote_StartPlay", PhotonTargets.Others, strCardSequence);
    }

    public void SendSelectHandCard(Card iCard)
    {
        iUI.ShowNetworkMessageForDebug("call SendSelectHandCard()");
        string strCard = iCard.GetCardString();
        photonView.RPC("Remote_SelectHandCard", PhotonTargets.Others, strCard);
    }

    public void SendGoOrStop(bool bGo)
    {
        iUI.ShowNetworkMessageForDebug("call SendGoOrStop()");
        photonView.RPC("Remote_DecideGoOrStop", PhotonTargets.Others, bGo);
    }

    [RPC]
    public void Remote_StartPlay(string strCardSequence)
    {
        iUI.ShowNetworkMessageForDebug("call Remote_StartPlay()");
        iUI.ShowNetworkMessage("");
        iGamePlay.MultiPlay_Post_JoiningRoom(strCardSequence);
    }

    [RPC]
    public void Remote_SelectHandCard(string strCard)
    {
        iUI.ShowNetworkMessageForDebug("call Remote_SelectHandCard()");
        iGamePlay.MultiPlay_Post_SelectHandCard(strCard);
    }

    [RPC]
    public void Remote_DecideGoOrStop(bool bGo)
    {
        iUI.ShowNetworkMessageForDebug("call Remote_DecidedGoOrStop()");
        iGamePlay.MultiPlay_DecideGoOrStop(bGo);
    }

}
