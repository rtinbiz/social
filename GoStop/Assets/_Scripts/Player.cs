﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public class Score {
		// Score
		static int MIN_WIN_SCORE = 7;

		public int nScore = 0;
		public int nLastWinScore = 0;

		public int nGoCount = 0;
		public bool bShake = false;

		public bool bOhKwang = false;
		public bool bGodori = false;
		public bool bHongdan = false;
		public bool bChungdan = false;
		public bool bChodan = false;
		public bool bMungtungguri = false;

		public void Clear() {
			nScore = 0;
			nLastWinScore = MIN_WIN_SCORE - 1;
			
			nGoCount = 0;
			bShake = false;
			
			bOhKwang = false;
			bGodori = false;
			bHongdan = false;
			bChungdan = false;
			bChodan = false;
			bMungtungguri = false;
		}
	}

    public enum SKILL_LEVEL
    {
        BEGINNER = 0,
        NORMAL = 1,
        EXPERT = 2
    }

	ArrayList iHandCards = new ArrayList();
	ArrayList iOwnCards = new ArrayList();

	public Score iScore = new Score ();

	public bool bMe = false;
    public bool bCanGoOrStop = false;

    static float MAX_TURN_COUNT_DOWN = 5.5f;
    public float nTurnCountDown = -1;
    public bool bTurnCountDown = false;

    SKILL_LEVEL nSkillLevel = SKILL_LEVEL.NORMAL;

	// Use this for initialization
	void Start () {
	
	}

	public void Clear () {
		iHandCards.Clear ();
		iOwnCards.Clear ();

		iScore.Clear ();

        bTurnCountDown = false;
        nTurnCountDown = 0;
	}
	
	public void AddHandCard(Card iCard) {
		iHandCards.Add(iCard);
	}

	public void AddOwnCard(Card iCard) {
		iOwnCards.Add(iCard);

        // Change Card Layer
        iCard.SetSortingLayer("OwnCard");
	}

	public void SortHandCards() {
		IComparer iComparer = new CardComparer();
		iHandCards.Sort (iComparer);
	}

	public bool RemoveHandCard(Card iCard) {
		if (iHandCards.Contains (iCard) == false) {
			return false;
		}

		iHandCards.Remove (iCard);
		return true;
	}

	public void SortOwnCards() {
		IComparer iComparer = new CardRankComparer();
		iOwnCards.Sort (iComparer);
	}

	void Update() {
		if (!CheckGameStatus ()) {
			return;
		}

        UpdateTurnCountDown();
	}
	
	bool CheckGameStatus() {
		GameManager.GAME_STATE nGameStatus = GameManager.GetInstance ().GetGameState ();
		if (nGameStatus == GameManager.GAME_STATE.PLAY) {
			return true;
		}
		
		return false;
	}

    void UpdateTurnCountDown() {
        if (!bTurnCountDown) {
            return;
        }

        nTurnCountDown -= Time.deltaTime;
        if (nTurnCountDown <= 0) {
            bTurnCountDown = false;
            Card iCard = SelectOneCard();
            GamePlay.GetInstance().PostEvent_OpenCard(iCard);
        }
    }

    public void StartTurnCountDown()
    {
        bTurnCountDown = true;
        nTurnCountDown = MAX_TURN_COUNT_DOWN;
    }

    public void StopTurnCountDown()
    {
        bTurnCountDown = false;
    }

	public void UpdateLayout() {
		UpdateHandCardsLayout ();
		UpdateOwnCardsLayout ();
	}

	public void UpdateHandCardsLayout() {
		SortHandCards ();

		Transform trHand = transform.Find ("Hand");

		Vector2 dLeftBottom = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 dRightTop = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		float nGapX = (dRightTop.x - dLeftBottom.x) / 12.0f;
		float nGapY = ((dRightTop.y - dLeftBottom.y) / Screen.height) * 15;
		float nOverlapX = nGapX / 4f;
		float nOffsetZ = -0.01f;

		bool bPrevBombCard = false;
		Card.SHAPE nPrevShape = Card.SHAPE.NONE;
		float nX = trHand.position.x;
		for (int i = 0; i < iHandCards.Count; i++) {
			Card iCard = (Card)iHandCards[i];
			iCard.transform.parent = trHand;

			float nOffsetY = 0;
			if (bMe) {
                if (iCard.bHasMatchedGroundCard) nOffsetY = nGapY;

                if (i == 0)
                {
					bPrevBombCard = iCard.bBomb;
					nPrevShape = iCard.nShape;
				}
				else {
					if(bPrevBombCard && iCard.bBomb && (nPrevShape == iCard.nShape)) {
						nX += nOverlapX;
					}
					else {
						nX += nGapX;
					}

					bPrevBombCard = iCard.bBomb;
					nPrevShape = iCard.nShape;
				}
			}
			else if(i != 0) {
				nX += nGapX;
            }

            iCard.MoveTo(new Vector3(nX, trHand.transform.position.y + nOffsetY, i * nOffsetZ));

			iCard.Show ();
			if(bMe) {
				iCard.Open();
			}
		}
	}

	public void CheckMatchedGroundCard(GroundCards iGroundCards, bool bClear = false) {
		for (int i = 0; i < iHandCards.Count; i++) {
			Card iCard = (Card)iHandCards[i];

			if(bClear) {
				iCard.bHasMatchedGroundCard = false;
			}
			else if(iCard.bJoker) {
				iCard.bHasMatchedGroundCard = true;
			}
			else if(iGroundCards.FindMatchedCard(iCard)) {
				iCard.bHasMatchedGroundCard = true;
			}
			else {
				iCard.bHasMatchedGroundCard = false;
			}
		}
	}

	public void UpdateOwnCardsLayout() {
		SortOwnCards ();

		Transform trOwn = transform.Find ("Own");

		Vector2 dLeftBottom = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 dRightTop = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		float nGapX = (dRightTop.x - dLeftBottom.x) / 12.0f;
		float nOverlapX = nGapX / 4f;
		float nOffsetZ = -0.01f;

		Card.RANK nPrevRank = Card.RANK.NONE;
		float nX = trOwn.position.x;
		for (int i = 0; i < iOwnCards.Count; i++) {
			Card iCard = (Card)iOwnCards[i];
			iCard.transform.parent = trOwn;

			if(i == 0) {
				nPrevRank = iCard.nRank;
			}
			else {
				if(nPrevRank < Card.RANK.THREEPI && nPrevRank != iCard.nRank) {
					nPrevRank = iCard.nRank;
					nX += nGapX;
				}
				else {
					nX += nOverlapX;
				}
			}

//			iCard.transform.position = new Vector3(nX, trOwn.transform.position.y, i * nOffsetZ);
            iCard.MoveTo(new Vector3(nX, trOwn.transform.position.y, i * nOffsetZ));

			iCard.Show ();
			iCard.Open();
		}
	}

	public Card SelectOneCard(bool bSelectLastOne = false) {
        Card iCard = null;
        if (nSkillLevel == SKILL_LEVEL.BEGINNER || bSelectLastOne)
        {
            if (iHandCards.Count >= 0)
            {
                int nIndex = iHandCards.Count - 1;
                iCard = (Card)iHandCards[nIndex];
                if (iCard.bBomb)
                {
                    ArrayList iBombCards = GetBombHandCards(iCard, false);
                    foreach (Card iBombCard in iBombCards)
                    {
                        iBombCard.Open();
                    }
                }

                iCard.Open();
            }
        }
        else    // NORMAL or EXPERT
        {
            if (iHandCards.Count >= 0)
            {
                bool bSelected = false;
                for (int i = iHandCards.Count - 1; i >= 0; i--)
                {
                    iCard = (Card)iHandCards[i];
                    if (iCard.bHasMatchedGroundCard)
                    {
                        if (iCard.bBomb)
                        {
                            ArrayList iBombCards = GetBombHandCards(iCard, false);
                            foreach (Card iBombCard in iBombCards)
                            {
                                iBombCard.Open();
                            }
                        }

                        iCard.Open();
                        bSelected = true;
                        break;
                    }
                }

                if (bSelected == false)
                {
                    return SelectOneCard(true);
                }
            }
        }

        return iCard;
	}

	public ArrayList GetBombHandCards(Card iBombCard, bool bRemove = true) {
		ArrayList iBombHandCards = new ArrayList ();

		foreach (Card iCard in iHandCards) {
			if (iCard.bBomb && iCard.nShape == iBombCard.nShape) {
				iBombHandCards.Add (iCard);
			}
		}

		if (bRemove) {
			foreach (Card iCard in iBombHandCards) {
				iHandCards.Remove (iCard);
			}
		}

		iBombHandCards.Add (iBombCard);

		return iBombHandCards;
	}

	public void CheckBombHandCards(GroundCards iGroundCards) {
		CheckShakeHandCards ();

		foreach(GroundCards.Slot iSlot in iGroundCards.iCardSlots) {
			Card iGroundCard = (Card)iSlot.GetFirstCard();
			if(iGroundCard) {
		        foreach(Card iCard in iHandCards) {
					if(iCard.bShake && iCard.nShape == iGroundCard.nShape) {
						iCard.bBomb = true;
					}
				}
			}
		}
	}

	public void CheckShakeHandCards() {
		Card.SHAPE nPrevShape = Card.SHAPE.NONE;
		int nBeginIndex = 0;
		for (int i = 0; i < iHandCards.Count; i++) {
			Card iCard = (Card)iHandCards [i];
			if (iCard.bJoker || iCard.bGhost) {
				continue;
			}

			iCard.bShake = false;

			if (nPrevShape != iCard.nShape) {
				nPrevShape = iCard.nShape;
				nBeginIndex = i;
			} else {
				if ((i - nBeginIndex) == 2) {
					for (int j = nBeginIndex; j <= i; j++) {
						Card iCard2 = (Card)iHandCards [j];
						iCard2.bShake = true;

						iScore.bShake = true;
					}
				}
			}
		}
	}

	public void GiveBonusPI(Player iOppenent, int nCount) {
		ArrayList iGiveCards = new ArrayList ();

		for (int i = 0; i < iOwnCards.Count; i++) {
			Card iCard = (Card)iOwnCards[i];
			if(iCard.nRank == Card.RANK.THREEPI && nCount >= 3) {
				nCount -= 3;
				iGiveCards.Add (iCard);
			}
			else if (iCard.nRank == Card.RANK.TWOPI && nCount >= 2) {
				nCount -= 2;
				iGiveCards.Add (iCard);
			}
			else if (iCard.nRank == Card.RANK.PI && nCount >= 1) {
				nCount --;
				iGiveCards.Add (iCard);
			}

			if(nCount <= 0) {
				break;
			}
		}

		foreach (Card iCard in iGiveCards) {
			iOppenent.AddOwnCard(iCard);
			iOwnCards.Remove(iCard);
		}

		// If there's still something to give, then give more PI even if it's over given amount
		if(nCount > 0) {
			iGiveCards.Clear();

			for (int i = iOwnCards.Count - 1; i >= 0; i--) {
				Card iCard = (Card)iOwnCards[i];
				if(iCard.nRank == Card.RANK.THREEPI) {
					nCount -= 3;
					iGiveCards.Add (iCard);
				}
				else if (iCard.nRank == Card.RANK.TWOPI) {
					nCount -= 2;
					iGiveCards.Add (iCard);
				}
				else if (iCard.nRank == Card.RANK.PI) {
					nCount --;
					iGiveCards.Add (iCard);
				}
				
				if(nCount <= 0) {
					break;
				}
			}

			foreach (Card iCard in iGiveCards) {
				iOppenent.AddOwnCard(iCard);
				iOwnCards.Remove(iCard);
			}
		}
	}

	public void CalculateScore() {
		int nCount_Kwang  = 0;
		bool bHasKwang_12 = false;
		int nCount_Godori = 0;
		int nCount_Hongdan = 0;
		int nCount_Chungdan = 0;
		int nCount_Chodan = 0;
		int nCount_Shape = 0;
		int nCount_Stripe = 0;
		int nCount_PI = 0;

		foreach (Card iCard in iOwnCards) {
			switch (iCard.nRank) {
			case Card.RANK.SHAPE: nCount_Shape++; break;
			case Card.RANK.STRIPE: nCount_Stripe++; break;
			case Card.RANK.THREEPI: 	nCount_PI += 3; break;
			case Card.RANK.TWOPI: nCount_PI += 2; break;
			case Card.RANK.PI: nCount_PI++; break;
			}

			switch(iCard.nSpecial) {
			case Card.SPECIAL.KWANG:
				nCount_Kwang++;
				if (iCard.nShape == Card.SHAPE._12) {
					bHasKwang_12 = true;
				}
				break;
			case Card.SPECIAL.GODORI : nCount_Godori++; break;
			case Card.SPECIAL.HONGDAN : nCount_Hongdan++; break;
			case Card.SPECIAL.CHUNGDAN : nCount_Chungdan++; break;
			case Card.SPECIAL.CHODAN : nCount_Chodan++; break;
			}
		}

		int nScore = 0;
		if (nCount_Kwang == 3) {
			if (bHasKwang_12)	 nScore += 2;
			else nScore += 3;
		} else if (nCount_Kwang == 4) {
			nScore += 4;
		} else if (nCount_Kwang == 5) {
			nScore += 15;
		}

		if (nCount_Godori == 3) nScore += 5;
		if (nCount_Hongdan == 3) nScore += 3;
		if (nCount_Chungdan == 3) nScore += 3;
		if (nCount_Chodan == 3) nScore += 3;
	
		if (nCount_Stripe >= 5) {
			nScore += (nCount_Stripe - 4);
		}

		if (nCount_Shape >= 5) {
			nScore += (nCount_Shape - 4);
		}

		if(nCount_PI >= 10) {
			nScore += (nCount_PI - 9);
		}

		// Check first GoOrStop with only score calculated by own cards.
		if (nScore > iScore.nLastWinScore) {
			bCanGoOrStop = true;
			iScore.nLastWinScore = nScore;
		} else {
			bCanGoOrStop = false;
		}

		if(iScore.nGoCount > 0) {
			if(iScore.nGoCount <= 2) {
				nScore += iScore.nGoCount;
			}
			else {
				nScore *= (int)Mathf.Pow(2, (iScore.nGoCount - 2));
			}
		}

		if (iScore.bShake) {
			nScore *= 2;
		}

		iScore.nScore = nScore;

		Debug.Log("========= Score, Me(" + bMe + ") : " + ", Score : " + iScore.nScore);
		Debug.Log("Count_Kwang : " + nCount_Kwang + ", Count_Godori : " + nCount_Godori + ", Count_Hongdan : " + nCount_Hongdan + ", Count_Chungdan : " + nCount_Chungdan + ", Count_Chodan : " + nCount_Chodan);
		Debug.Log("Count_Shape : " + nCount_Shape + ", Count_Stripe : " + nCount_Stripe + ", Count_PI : " + nCount_PI);
		Debug.Log ("Shake : " + iScore.bShake + ", GoCount : " + iScore.nGoCount + ", LastWinScore : " + iScore.nLastWinScore + ", WinScore : " + nScore);
	}

	public void DecidedGo() {
		iScore.nGoCount++;
	}

	public bool isEmptyHandCards() {
		return (iHandCards.Count == 0);
	}

    public Card FindCard(string strCard) {
        string strShape= strCard.Substring(0, 2);
        string strIndex = strCard.Substring(2, 2);

        Card.SHAPE nShape = (Card.SHAPE)int.Parse(strShape);
        Card.INDEX nIndex = (Card.INDEX)int.Parse(strIndex);

        foreach (Card iCard in iHandCards)
        {
            if (iCard.nShape == nShape && iCard.nIndex == nIndex)
            {
                return iCard;
            }
        }

        return null;
    }
}
