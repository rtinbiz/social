﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {
	private GameManager iGameManager;
    private GamePlay iGamePlay;
	public Player iMe;
	public Player iYou;

    public bool bShowDebugMessage = false;

    private static UI _this;
    public static UI GetInstance()
    {
        return _this;
    }

	// Use this for initialization
	void Awake() {
        _this = this;
    }

    void Init() {
        iGameManager = GameManager.GetInstance();
        iGamePlay = GamePlay.GetInstance();
	}

	void Start () {
        Init();
	}

    public void Clear()
    {
        UpdateScore();

        ShowNetworkMessage("");
        ShowNetworkMessageForDebug("");
    }
	
	// Update is called once per frame
	void Update () {
		UpdateUI ();
	}

	void UpdateUI () {
		GamePlay.PLAY_STATE nPlayStatus = iGamePlay.GetPlayState ();
		Transform trStartMenu = transform.FindChild ("StartMenu");
        if (trStartMenu) {
            trStartMenu.gameObject.SetActive(false);
            if (nPlayStatus == GamePlay.PLAY_STATE.READY)
            {
                trStartMenu.gameObject.SetActive(true);
                trStartMenu.FindChild("Button_SinglePlay").gameObject.SetActive(true);
                trStartMenu.FindChild("Button_MultiPlay").gameObject.SetActive(true);
                trStartMenu.FindChild("Button_Reconnect").gameObject.SetActive(false);
            }
            if (nPlayStatus == GamePlay.PLAY_STATE.JOINING_ROOM)
            {
                trStartMenu.gameObject.SetActive(true);
                trStartMenu.FindChild("Button_SinglePlay").gameObject.SetActive(false);
                trStartMenu.FindChild("Button_MultiPlay").gameObject.SetActive(false);
                trStartMenu.FindChild("Button_Reconnect").gameObject.SetActive(true);
            }
		}

        UpdateTurnCountDown();
	}

	public void UpdateScore() {
		Transform trPlayer = transform.Find ("Score/Me");
		if (iMe.iScore.nScore > 0) {
			trPlayer.gameObject.SetActive (true);
			Text iText = trPlayer.Find ("Text").GetComponent<Text>();
			iText.text = "Score : " + iMe.iScore.nScore;
		} else {
			trPlayer.gameObject.SetActive (false);
		}

		trPlayer = transform.Find ("Score/You");
		if (iYou.iScore.nScore > 0) {
			trPlayer.gameObject.SetActive (true);
			Text iText = trPlayer.Find ("Text").GetComponent<Text>();
			iText.text = "Score : " + iYou.iScore.nScore;
		} else {
			trPlayer.gameObject.SetActive (false);
		}
	}

    public void UpdateTurnCountDown()
    {
        Transform trPlayer = transform.Find("Turn/Me");
        if (iMe.bTurnCountDown) { 
            if (iMe.nTurnCountDown > 0) {
                trPlayer.gameObject.SetActive(true);
                Text iText = trPlayer.Find("Text").GetComponent<Text>();
                iText.text = iMe.nTurnCountDown.ToString("0");
            }
        }
        else
        {
            trPlayer.gameObject.SetActive(false);
        }

        if (iGamePlay.bMultiPlay)
        {
            trPlayer = transform.Find("Turn/You");
            if (iYou.bTurnCountDown)
            {
                if (iYou.nTurnCountDown > 0)
                {
                    trPlayer.gameObject.SetActive(true);
                    Text iText = trPlayer.Find("Text").GetComponent<Text>();
                    iText.text = iYou.nTurnCountDown.ToString("0");
                }
            }
            else
            {
                trPlayer.gameObject.SetActive(false);
            }
        }
    }

	public void ShowGoOrStopBox(bool bShow = true) {
		Transform trBox = transform.Find ("GoStop");
		trBox.gameObject.SetActive (bShow);
	}

    public void ShowGoOrStopWaitingBox(bool bShow = true)
    {
        Transform trBox = transform.Find("GoStopWaiting");
        trBox.gameObject.SetActive(bShow);
    }

	public void OnClickButton_GO() {
		GamePlay.GetInstance ().PostEvent_DecideGoOrStop (this);
	}

	public void OnClickButton_Stop() {
		GamePlay.GetInstance ().PostEvent_DecideGoOrStop (false);
	}

    public void OnClickButton_SinglePlay()
    {
        iGamePlay.SetMultiPlay(false);
        iGameManager.SetGameState(GameManager.GAME_STATE.START);
    }

    public void OnClickButton_MultiPlay()
    {
        iGamePlay.SetMultiPlay(true);
        iGameManager.SetGameState(GameManager.GAME_STATE.START);
    }

    public void OnClickButton_Reconnect()
    {
        iGamePlay.SetPlayState(GamePlay.PLAY_STATE.END);
        OnClickButton_MultiPlay();
    }

    public void ShowNetworkMessage(string strMessage)
    {
        Text iText = transform.Find("NetworkMessage").GetComponent<Text>();
        if (strMessage.Length > 0) {
            iText.gameObject.SetActive(true);
            iText.text = strMessage;
        }
        else
        {
            iText.gameObject.SetActive(false);
        }
    }

    public void ShowNetworkMessageForDebug(string strMessage)
    {
        Text iText = transform.Find("NetworkMessageForDebug").GetComponent<Text>();
        if (bShowDebugMessage && strMessage.Length > 0)
        {
            iText.gameObject.SetActive(true);
            iText.text = strMessage;
        }
        else
        {
            iText.gameObject.SetActive(false);
        }
    }

}
