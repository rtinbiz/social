﻿using UnityEngine;
using System.Collections;

public class GamePlay : MonoBehaviour
{
	public enum PLAY_STATE {
		READY = 0,
        START_PLAY,
        SHUFFLE_CARD,
		DITSRIBUTE_CARD,
		SELECT_HAND_CARD,
		OPEN_DECK_CARD,
		DECIDE_GO_OR_STOP,
		CHANGE_TURN,
        JOINING_ROOM,
		END
	};

	private PLAY_STATE nPlayState = PLAY_STATE.READY;

	private int PLAYER_HAND_CARD_COUNT = 10;
	private int GROUND_CARD_COUNT = 8;

	private GameManager iGameManager;
	private CardDeck iCardDeck;
	private GroundCards iGroundCards;
	private Player iMe;
	private Player iYou;
	private UI iUI;
    private Network iNetwork;

	public bool bMyTurn = true;
    public bool bMultiPlay = false;
    public bool bHost = true;

	private static GamePlay _this;
	public static GamePlay GetInstance() {
		return _this;
	}
	
	void Awake() {
		_this = this;
	}

	public void Init() {
		iGameManager = GameManager.GetInstance ();
		iCardDeck = transform.Find ("CardDeck").GetComponent<CardDeck> ();
		iGroundCards = transform.Find ("GroundCards").GetComponent<GroundCards> ();
		iMe = transform.Find ("Player/Me").GetComponent<Player> ();
		iYou = transform.Find ("Player/You").GetComponent<Player> ();
		iUI = transform.Find ("UI").GetComponent<UI> ();
        iNetwork = transform.Find("Network").GetComponent<Network>();
    }

	public void Clear() {
		iCardDeck.Clear ();
		iMe.Clear ();
		iYou.Clear ();
		iGroundCards.Clear ();

        iUI.Clear();
	}

	void Start() {
		Init ();
		SetPlayState (PLAY_STATE.READY);
	}

	void Update() {
		if (!CheckGameState ()) {
			return;
		}
	}


	bool CheckGameState() {
		GameManager.GAME_STATE nGameState = iGameManager.GetGameState ();
		if (nGameState == GameManager.GAME_STATE.PLAY) {
			return true;
		}

		return false;
	}

	public void SetPlayState (PLAY_STATE nPS) {
		nPlayState = nPS;

	   switch (nPlayState) {
		case PLAY_STATE.READY:
			PlayState_Ready ();
			break;
        case PLAY_STATE.START_PLAY:
            PlayState_StartPlay();
            break;
        case PLAY_STATE.JOINING_ROOM:
            PlayState_JoiningRoom();
            break;
        case PLAY_STATE.SHUFFLE_CARD :
			PlayState_ShuffleCards ();
			break;
		case PLAY_STATE.DITSRIBUTE_CARD:
			PlayState_DistributeCard ();
			break;
		case PLAY_STATE.SELECT_HAND_CARD: 
			PlayState_SelectHandCard ();
			break;
		case PLAY_STATE.OPEN_DECK_CARD:
			PlayState_OpenDeckCard ();
			break;
		case PLAY_STATE.DECIDE_GO_OR_STOP:
			PlayState_DecideGoOrStop ();
			break;
		case PLAY_STATE.CHANGE_TURN:
			PlayState_ChangeTurn ();
			break;
		case 	PLAY_STATE.END:
			PlayState_End ();
			break;
		}
	}
	
	public PLAY_STATE GetPlayState () {
		return nPlayState;
	}

	void PlayState_Ready() {
	}

    public void PlayState_StartPlay()
    {
        Clear();

        if (bMultiPlay)
        {
            MultiPlay_StartPlay();
            return;
        }

        bMyTurn = true;
        SetPlayState(PLAY_STATE.SHUFFLE_CARD);
    }

    public void MultiPlay_StartPlay()
    {
        SetPlayState(PLAY_STATE.JOINING_ROOM);
    }

    void PlayState_JoiningRoom()
    {
        iNetwork.Connect();
    }

    public void MultiPlay_Post_JoiningRoom(string strCardSequence = "")
    {
        if (!bHost)
        {
            iCardDeck.SetShuffleCards(strCardSequence);
        }
        SetPlayState(PLAY_STATE.SHUFFLE_CARD);
    }

	void PlayState_ShuffleCards ()
    {
        if (!bMultiPlay || (bMultiPlay && bHost))
        {
            iCardDeck.ShuffleCards();
            iNetwork.SendCardDeck(iCardDeck);
        }

        iCardDeck.ShowShuffleEffect();
    }

    public void Post_PlayState_ShuffleCards()
    {
        SetPlayState(PLAY_STATE.DITSRIBUTE_CARD);
    }

	void PlayState_DistributeCard () {
        if (bHost)
        {
            iCardDeck.DistributeCards(iMe, PLAYER_HAND_CARD_COUNT);
            iCardDeck.DistributeCards(iYou, PLAYER_HAND_CARD_COUNT);
            iCardDeck.DistributeCards(iGroundCards, iMe, GROUND_CARD_COUNT);
        }
        else
        {
            iCardDeck.DistributeCards(iYou, PLAYER_HAND_CARD_COUNT);
            iCardDeck.DistributeCards(iMe, PLAYER_HAND_CARD_COUNT);
            iCardDeck.DistributeCards(iGroundCards, iYou, GROUND_CARD_COUNT);
        }

		iGroundCards.CheckBombCards ();

		iMe.SortHandCards ();
		iYou.SortHandCards ();

		iMe.UpdateLayout ();
		iYou.UpdateLayout ();
		iGroundCards.UpdateLayout ();
		iCardDeck.UpdateLayout ();
		
		SetPlayState (PLAY_STATE.SELECT_HAND_CARD);
	}

    void PlayState_SelectHandCard() {
        if (bMultiPlay) {
            MultiPlay_PlayState_SelectHandCard();
            return;
        }

		Player iPlayer = bMyTurn ? iMe : iYou;

        iPlayer.StartTurnCountDown();
		iPlayer.CheckBombHandCards (iGroundCards);
        iPlayer.CheckMatchedGroundCard(iGroundCards);

		if (bMyTurn) {
			iPlayer.UpdateHandCardsLayout ();
		} else {
            Card iCard = iPlayer.SelectOneCard();
            StartCoroutine(WaitAndProcessOpenCard(iCard));
        }
	}

    void MultiPlay_PlayState_SelectHandCard()
    {
        Player iPlayer = bMyTurn ? iMe : iYou;
        iPlayer.StartTurnCountDown();
        iPlayer.CheckBombHandCards(iGroundCards);
        iPlayer.CheckMatchedGroundCard(iGroundCards);
        iPlayer.UpdateHandCardsLayout();
    }

    public void MultiPlay_Post_SelectHandCard(string strCard)
    {
        if (bMyTurn) {
            return;
        }

        Card iCard = iYou.FindCard(strCard);
        ProcessSelectCard(iCard);
    }

	void PlayState_OpenDeckCard() {
		Card iCard = iCardDeck.OpenTopCard ();
        StartCoroutine(WaitAndProcessOpenCard(iCard));
	}

    IEnumerator WaitAndProcessOpenCard(Card iCard)
    {
		yield return new WaitForSeconds (1f);
        
        PostEvent_OpenCard(iCard);
    }
    
	void PlayState_DecideGoOrStop() {
		Player iPlayer = bMyTurn ? iMe : iYou;

		iPlayer.CalculateScore ();
		iUI.UpdateScore ();

		if (iCardDeck.isEmpty () || iPlayer.isEmptyHandCards()) {
			SetPlayState(PLAY_STATE.END);
			return;
		}

		if (iPlayer.bCanGoOrStop) {
			if(bMyTurn) {
				iUI.ShowGoOrStopBox();
            }
            else
            {
                if (bMultiPlay)
                {
                    iUI.ShowGoOrStopWaitingBox();
                }
                else
                {
                    SetPlayState(PLAY_STATE.END);
                }
			}
		} else {
			SetPlayState (PLAY_STATE.CHANGE_TURN);
		}
	}

	void PlayState_ChangeTurn() {
		bMyTurn = !bMyTurn;
		SetPlayState(PLAY_STATE.SELECT_HAND_CARD);
	}

    void PlayState_End()
    {
        iGameManager.SetGameState(GameManager.GAME_STATE.END);
        if (bMultiPlay)
        {
            iNetwork.Disconnect();
        }
	}

	public void PostEvent_SelectCard(Card iCard) {
		if (nPlayState != PLAY_STATE.SELECT_HAND_CARD) {
			return;
        }

        if (bMultiPlay) {
            if (bMyTurn) iNetwork.SendSelectHandCard(iCard);
            else return;
        }

        ProcessSelectCard(iCard);
    }

    void ProcessSelectCard(Card iCard) {
        Player iPlayer = bMyTurn ? iMe : iYou;
        iPlayer.StopTurnCountDown();

		if (!iPlayer.RemoveHandCard(iCard) ) {
			return;
		}

		if (iCard.bGhost) {
            if (bMultiPlay && bMyTurn) {
                iNetwork.SendSelectHandCard(iCard);
            }

			iCardDeck.DestroyGhostCard(iCard);

			SetPlayState (PLAY_STATE.OPEN_DECK_CARD);
			return;
		}

		if (iCard.bJoker) {
			iPlayer.AddOwnCard(iCard);
			Card iNewCard = iCardDeck.GetNextCard();
			iPlayer.AddHandCard(iNewCard);

			iPlayer.UpdateLayout();
			SetPlayState(PLAY_STATE.SELECT_HAND_CARD);
			return;
		}

		Card iMatchedCard = iGroundCards.FindMatchedCard (iCard);
		if (iMatchedCard) {
			if(iCard.bBomb) {
				iGroundCards.AddMatchedCards (iMatchedCard, iPlayer.GetBombHandCards(iCard));
				// Add two ghost cards
				iPlayer.AddHandCard (iCardDeck.CreateGhostCard(bMyTurn));
				iPlayer.AddHandCard (iCardDeck.CreateGhostCard(bMyTurn));
            }
			else {
				iGroundCards.AddMatchedCard (iMatchedCard, iCard);
			}
		} else {
			iGroundCards.Add (iCard);
		}

		iPlayer.UpdateOwnCardsLayout ();
        iGroundCards.UpdateLayout();
		SetPlayState (PLAY_STATE.OPEN_DECK_CARD);
	}

	public void PostEvent_OpenCard(Card iCard) {
		if (nPlayState == PLAY_STATE.OPEN_DECK_CARD) {
			StartCoroutine (PostOpenDeckCard (iCard));
		}
		else if (nPlayState == PLAY_STATE.SELECT_HAND_CARD) {
			PostEvent_SelectCard (iCard);
		}
	}

	IEnumerator PostOpenDeckCard(Card iCard) {
		Player iPlayer = bMyTurn ? iMe : iYou;
		Player iOppenent = bMyTurn ? iYou : iMe;
		if (iCard.bJoker) {
			Card iMatchedCard = iGroundCards.GetMatchedCard ();
			if(iMatchedCard) {
				iGroundCards.AddMatchedCard (iMatchedCard, iCard);
				iGroundCards.UpdateLayout();
			}
			else {
				iPlayer.AddOwnCard(iCard);
				iPlayer.UpdateOwnCardsLayout ();
			}

			SetPlayState(PLAY_STATE.OPEN_DECK_CARD);
		} else {
			Card iMatchedCard = iGroundCards.FindMatchedCard (iCard);
			if (iMatchedCard) {
				iGroundCards.AddMatchedCard (iMatchedCard, iCard);
			} else {
				iGroundCards.Add (iCard);
			}

			iGroundCards.UpdateLayout ();

			yield return new WaitForSeconds (1);

			int nCountBonusPI = iGroundCards.CheckBonusPICount();
			if(nCountBonusPI > 0) iOppenent.GiveBonusPI(iPlayer, nCountBonusPI);
			iGroundCards.MoveMatchedCardToPlayer (iPlayer);
			
			iPlayer.CheckBombHandCards (iGroundCards);
			iPlayer.CheckMatchedGroundCard (iGroundCards);
			iPlayer.UpdateLayout ();
			iGroundCards.UpdateLayout ();
			
			SetPlayState (PLAY_STATE.DECIDE_GO_OR_STOP);
		}
	}

	public void PostEvent_DecideGoOrStop(bool bGo) {
		if (!bMyTurn) {
			return;
		}

        if (bMultiPlay)
        {
            iNetwork.SendGoOrStop(bGo);
        }

        iUI.ShowGoOrStopBox(false);

		if (bGo) {
			iMe.DecidedGo ();
			SetPlayState(PLAY_STATE.CHANGE_TURN);
		} else {
			SetPlayState(PLAY_STATE.END);
		}
	}

    public void MultiPlay_DecideGoOrStop(bool bGo)
    {
        if (bMyTurn)
        {
            return;
        }

        iUI.ShowGoOrStopWaitingBox(false);

        if (bGo)
        {
            iYou.DecidedGo();
            SetPlayState(PLAY_STATE.CHANGE_TURN);
        }
        else
        {
            SetPlayState(PLAY_STATE.END);
        }
    }

     public void SetMultiPlay(bool bSet) {
         bMultiPlay = bSet;
    }

     public void SetHostPlayer(bool bSet)
     {
         bHost = bSet;
         bMyTurn = bHost;
     }

     public void MultiPlay_Disconnected()
     {
         iUI.ShowGoOrStopBox(false);
         iUI.ShowGoOrStopWaitingBox(false);

         SetPlayState(PLAY_STATE.END);
     }
}
