﻿using UnityEngine;
using System.Collections;

public class CardDeck : MonoBehaviour {
	static int CARD_COUNT = 48;
	static int JOKER_CARD_CARD = 2;
	static int TOTAL_COUNT_CARD = CARD_COUNT + JOKER_CARD_CARD;

	public GameObject pfCard;
	private ArrayList iCards = new ArrayList();
	private ArrayList iGhostCards = new ArrayList();
	private Sprite iSpriteGhostCard = null;
    private int nGhostCardIndex = 0;
	private int[] iCardSequence = new int[TOTAL_COUNT_CARD];
	private int nCardSequenceIndex = 0;

	void Awake() {
		Init ();
	}

	void Init() {
		Sprite[] listSprite = Resources.LoadAll<Sprite> ("Sprites/Card");

		Card.SHAPE nShape;
		Card.INDEX nIndex;
		string[] strSubStrings;
		for (int i = 0; i < listSprite.Length; i++) {
			Sprite iSprite = listSprite[i];

			if(iSprite.name.Contains("Card_Ghost")) {
				iSpriteGhostCard = iSprite;
			} 
			else if(iSprite.name.Contains("Card_Joker")) {
				strSubStrings = iSprite.name.Split('_');
				nIndex = (Card.INDEX) int.Parse(strSubStrings[2]);

				CreateCard(iSprite, Card.SHAPE.JOKER, nIndex);
			}
			else if(!iSprite.name.Contains("Card_Back")) {
				strSubStrings = iSprite.name.Split('_');
				int nNumber =	int.Parse(strSubStrings[1]);
				nShape = (Card.SHAPE) (nNumber / 4);
				nIndex = (Card.INDEX) (nNumber % 4);

				CreateCard(iSprite, nShape, nIndex);
			}
		}

        // Shuffle initally.
        ShuffleCards();
	}

	void CreateCard(Sprite iSprite, Card.SHAPE nShape, Card.INDEX nIndex) {
		GameObject goCard = Instantiate(pfCard, transform.position, transform.rotation) as GameObject;
		goCard.transform.parent = transform;
        goCard.transform.position = new Vector3(0f, 0f, 0f);
		goCard.transform.localScale = new Vector3(1.6f, 1.6f, 1f);
		goCard.SetActive(false);
		goCard.GetComponent<SpriteRenderer>().sprite = iSprite;

		Card iCard = goCard.GetComponent<Card> ();
		iCard.nShape = nShape;
		iCard.nIndex = nIndex;

		if (nShape == Card.SHAPE.JOKER) {
			iCard.bJoker = true;
			iCard.nRank = Card.RANK.TWOPI;
		} else {
			iCard.nRank = Card.dRankMap [(int)nShape, (int)nIndex];
			iCard.nSpecial = Card.dSpecialMap [(int)nShape, (int)nIndex];
		}

		iCards.Add (iCard);
	}

	public Card CreateGhostCard(bool bRemoveBackCard = false) {
		Transform trGhostCards = transform.Find ("GhostCards");
		GameObject goCard = Instantiate(pfCard, transform.position, transform.rotation) as GameObject;
		goCard.transform.parent = trGhostCards;
        goCard.transform.position = new Vector3(0f, 0f, 0f);
        goCard.transform.localScale = new Vector3(1.6f, 1.6f, 1f);
		goCard.SetActive(false);
		goCard.GetComponent<SpriteRenderer>().sprite = iSpriteGhostCard;
		goCard.GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 0.25f);

		Card iCard = goCard.GetComponent<Card> ();
		iCard.nShape = Card.SHAPE.GHOST;
        iCard.nIndex = (Card.INDEX)nGhostCardIndex++;
        iCard.nRank = Card.RANK.NONE;
        iCard.bGhost = true;

		Transform trCardBack = goCard.transform.Find ("CardBack");
		if (bRemoveBackCard) {
			DestroyObject (trCardBack.gameObject);
		} else {
			trCardBack.gameObject.GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 0.5f);
		}

		iGhostCards.Add (iCard);

		return iCard;
	}

	public void DestroyGhostCard(Card iCard) {
		iGhostCards.Remove (iCard);
		DestroyObject (iCard.gameObject);
	}

	public void Clear() {
		foreach(Card iCard in iCards) {
			iCard.Clear();

			iCard.transform.parent = transform;
//			iCard.transform.position = transform.position;
            iCard.MoveTo(transform.position);
			iCard.Open (false);
		}

		foreach(Card iCard in iGhostCards) {
			DestroyObject (iCard.gameObject);
		}
		iGhostCards.Clear ();
        nGhostCardIndex = 0;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShuffleCards () 
	{ 
		for (int i = 0; i < iCards.Count; i++) {
			iCardSequence [i] = i;
		}

		// for test
/*
		{
			int nIndex1 = 3;
			int nIndex2 = 21;
			int nTemp2;
			nTemp2 = iCardSequence [nIndex1];
			iCardSequence [nIndex1] = iCardSequence[nIndex2];
			iCardSequence [nIndex2] = nTemp2;

            nIndex1 = 7;
            nIndex2 = 10;
            nTemp2 = iCardSequence[nIndex1];
            iCardSequence[nIndex1] = iCardSequence[nIndex2];
            iCardSequence[nIndex2] = nTemp2;

            nIndex1 = 10;
            nIndex2 = 22;
            nTemp2 = iCardSequence[nIndex1];
            iCardSequence[nIndex1] = iCardSequence[nIndex2];
            iCardSequence[nIndex2] = nTemp2;

            nIndex1 = 11;
            nIndex2 = 23;
            nTemp2 = iCardSequence[nIndex1];
            iCardSequence[nIndex1] = iCardSequence[nIndex2];
            iCardSequence[nIndex2] = nTemp2;
        }


		nCardSequenceIndex = 0;
		return;
*/

		int nRandomIndex;
		int nTemp;
		for (int i = 0; i < iCards.Count; i++) {
			nRandomIndex = Random.Range (i, TOTAL_COUNT_CARD);

			nTemp = iCardSequence [i];
			iCardSequence [i] = iCardSequence[nRandomIndex];
			iCardSequence [nRandomIndex] = nTemp;
		}			

		nCardSequenceIndex = 0;
	} 

	public Card GetNextCard() {
		if(nCardSequenceIndex >= iCards.Count) {
			return null;
		}

		int nIndex = iCardSequence [nCardSequenceIndex];
		nCardSequenceIndex++;

		return (Card)iCards [nIndex];
	}

	public void UpdateLayout () {
		Vector2 dLeftBottom = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 dRightTop = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		float nOffsetX = ((dRightTop.x - dLeftBottom.x) / Screen.width)/2;
		float nOffsetY = (dRightTop.y - dLeftBottom.y) / Screen.height;
		float nOffsetZ = 0.01f;

		int nOffsetIndex = 0;
		for (int i = iCards.Count - 1; i >= nCardSequenceIndex; i--) {
			int nIndex = iCardSequence [i];
			Card iCard = (Card)iCards[nIndex];
            iCard.MoveTo(new Vector3(transform.position.x + nOffsetIndex * nOffsetX, transform.position.y + nOffsetIndex * nOffsetY, transform.position.z - nOffsetIndex * nOffsetZ));
			iCard.Show ();

			nOffsetIndex++;
		}
	}

    public void UpdateSuffleLayout()
    {
        Vector2 dLeftBottom = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 dRightTop = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        float nOffsetZ = 0.01f;

        int nOffsetIndex = 0;
        for (int i = iCards.Count - 1; i >= nCardSequenceIndex; i--)
        {
            float nOffsetX = Random.Range(-6f, 6f);
            float nOffsetY = Random.Range(-4f, 4f);

            int nIndex = iCardSequence[i];
            Card iCard = (Card)iCards[nIndex];
            iCard.MoveTo(new Vector3(nOffsetX, nOffsetY, transform.position.z - nOffsetIndex * nOffsetZ));
            iCard.Show();

            nOffsetIndex++;
        }
    }

	public void DistributeCards (Player iPlayer, int nCardsCount) {
		for (int i = 0; i < nCardsCount; i++) {
			Card iCard = GetNextCard();
			iCard.bInHand = true;
			iPlayer.AddHandCard(iCard);
		}
	}

	public void DistributeCards (GroundCards iGroundCards, Player iPlayer, int nCardsCount) {
		for (int i = 0; i < nCardsCount; i++) {
			Card iCard = GetNextCard();
            if (iCard.bJoker)
            {
				iPlayer.AddOwnCard(iCard);
				i--;
			}
			else {
				iGroundCards.Add(iCard);
			}
		}
	}

	public Card OpenTopCard() {
		Card iCard = GetNextCard ();
		if (iCard == null) {
			return null;
		}

		iCard.Open ();
		return iCard;
	}

	public bool isEmpty() {
		if(nCardSequenceIndex >= iCards.Count) {
			return true;
		}	
	
		return false;
	}

    public string GetShuffleCards()
    {
        string strIndex;
        string strSequence = "";

        for (int i = 0; i < TOTAL_COUNT_CARD; i++)
        {
            int nIndex = iCardSequence[i];
            if (nIndex < 10) strIndex = "0" + nIndex;
            else strIndex = "" + nIndex;
            strSequence += strIndex;
        }

        return strSequence;
    }

    public void SetShuffleCards(string strCardSequence)
    {
        string strIndex;
        for (int i = 0; i < TOTAL_COUNT_CARD; i++)
        {
            strIndex = strCardSequence.Substring(i * 2, 2);
            iCardSequence[i] = int.Parse(strIndex);
        }

        nCardSequenceIndex = 0;
    }

    public void ShowShuffleEffect()
    {
        StartCoroutine(ShuffleAndGatherDeckCards());
    }

    IEnumerator ShuffleAndGatherDeckCards()
    {
        UpdateSuffleLayout();
        yield return new WaitForSeconds(1);

        UpdateLayout();
        yield return new WaitForSeconds(1);

        GamePlay.GetInstance().Post_PlayState_ShuffleCards();
    }
}
